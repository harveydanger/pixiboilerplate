const mix = require('laravel-mix');

mix.js('src/js/app.js', 'dist/js')
   .sass('src/sass/app.scss', 'dist/css')
   .copy('src/index.html', 'dist/')
   .copy('src/assets', 'dist/assets')
   .browserSync({
    proxy: false,
    files: [
        'dist/css/{*,**/*}.css',
        'dist/js/{*,**/*}.js',
        'dist/{*,**/*}.html'
    ],
    server: {
        baseDir: './dist'
    },
    // index: "index.html",
    ui: {
        port: 8080,
        weinre: {
            port: 9090
        }
    }
});